/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

/**
 *
 * @author angel
 */
public class Pago {
    private int numDocente;
    private int nivel;
    private int horas;
    private String nombre;
    private String domicilio;
    private float pagoBase;

    public Pago() {
        this.numDocente = 0;
        this.nivel = 0;
        this.horas = 0;
        this.nombre = "Sin asignar";
        this.domicilio = "Sin asignar";
        this.pagoBase = 0.0f;
    }

    public Pago(int numDocente, int nivel, int horas, String nombre, String domicilio, float pagoBase) {
        this.numDocente = numDocente;
        this.nivel = nivel;
        this.horas = horas;
        this.nombre = nombre;
        this.domicilio = domicilio;
        this.pagoBase = pagoBase;
    }
    
    public Pago(Pago x) {
        this.numDocente = x.numDocente;
        this.nivel = x.nivel;
        this.horas = x.horas;
        this.nombre = x.nombre;
        this.domicilio = x.domicilio;
        this.pagoBase = x.pagoBase;
    }

    public int getNumDocente() {
        return numDocente;
    }

    public void setNumDocente(int numDocente) {
        this.numDocente = numDocente;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public int getHoras() {
        return horas;
    }

    public void setHoras(int horas) {
        this.horas = horas;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public float getPagoBase() {
        return pagoBase;
    }

    public void setPagoBase(float pagoBase) {
        this.pagoBase = pagoBase;
    }
    
    float calcularPago(){
        float pago = 0.0f;
        switch(this.nivel){
            case 1:
                pago = this.pagoBase * 1.3f * this.horas;
                break;
            case 2:
                pago = this.pagoBase * 1.5f * this.horas;
                break;
            case 3:
                pago = this.pagoBase * 2f * this.horas;
                break;
        }
        return pago;
    }
    
    float calcularImpuesto(){
        float impuesto = 0.0f;
        impuesto = this.calcularPago() * 0.16f;
        return impuesto;
    }
    
    float calcularBono(int numHijos){
        float bono = 0.0f;
       switch(numHijos){
           case 0: 
               bono = 0.0f;
               break;
           case 1:
            bono = this.calcularPago() * 0.05f;
            break;
           case 2:
             bono = this.calcularPago() * 0.05f;
            break;  
           case 3:
            bono = this.calcularPago() * 0.1f;
            break;
           case 4:
            bono = this.calcularPago() * 0.1f;
            break;
           default:
            bono = this.calcularPago() * 0.2f;
            break;
        }
        return bono;
    }
    
}
